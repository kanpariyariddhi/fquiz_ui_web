import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./Quiz/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'info-quiz',
    loadChildren: () => import('./Quiz/infoQuiz/infoquiz.module').then( m => m.InfoPageModule)
  },
  {
    path: 'question_quiz',
    loadChildren: () => import('./Quiz/question_quiz/question_quiz.module').then( m => m.QuestionQuizPageModule)
  },
  {
    path: '',
    redirectTo: 'question_quiz',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
