import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-infoquiz',
  templateUrl: 'infoquiz.page.html',
  styleUrls: ['infoquiz.page.scss'],
})
export class InfoPage {

  constructor(private route: Router) {}
  quizBtn(){
    this.route.navigate(['/otp']);
  }

}
