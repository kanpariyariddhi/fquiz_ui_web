import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { QuestionQuizPage } from './question_quiz.page';

import { QuestionQuizPageRoutingModule } from './question_quiz-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    QuestionQuizPageRoutingModule
  ],
  declarations: [QuestionQuizPage]
})
export class QuestionQuizPageModule {}
