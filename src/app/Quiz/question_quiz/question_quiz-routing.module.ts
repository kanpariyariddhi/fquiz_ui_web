import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { QuestionQuizPage } from './question_quiz.page';

const routes: Routes = [
  {
    path: '',
    component: QuestionQuizPage,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuestionQuizPageRoutingModule {}
